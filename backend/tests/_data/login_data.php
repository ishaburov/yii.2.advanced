<?php
return [
    [
        'username' => 'admin',
        'auth_key' => 'tt-LvwCdv2K7jwKgRMaNbm43WKspXVsU',
        // password_0
        'password_hash' => '$2y$13$3l/e7oyaPIYmXkVACefrnusmFrk8.x.qJopUIpC3UwmPFzH.Pabzi',
        'password_reset_token' => null,
        'created_at' => '1543133698',
        'updated_at' => '1543133698',
        'email' => 'admin@test.com',
    ],
];
